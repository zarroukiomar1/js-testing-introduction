const { generateText, createElement, validateInput } = require('./util');
const sshkey = `-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,464272B68A250F57

Hmt+L7+Uba5qBifs2TLAiA6SG0oRRfmUfVYh4rkVigaRQu0BOqvCWmIGj6BxQmdB
MqBaCK0m7a/zfkpNbH4udHrDAoJW3/hKGjcOoGaoTtAYRFKV27f4iv627Kmc4JVm
vsQp5juYmlYtkb9hjsCIF2UtYpxIW2uw/X9H2iIqWYfNUQY83u8kYeQlgQvWEInc
1ynbbR7grbs0/s7JW85677d8v5jwvsmKoIoXpqYbnVhvEq1Najp2yENb6+RehyHU
tAnow8nNUmoJBZU+qW1Q1TPM913+jocuN4troPFb/1tccuzIWQuaDnKQPG8LURSe
OHHtV9R3eDC1OJwY12isAvrfmh1uNrm45L3WG6E81JO0VhoRLMH+g2bSSo1hqftz
26f7waYlZPuHf/4n2VtZLy/w7+vZdbrUgcl7tKCFo6zoFVws1XcYhnqS0Cu2l5l2
JyTMu63CNef84+cg8HxjXrvlOrUwCrNMJOfiD8SzYXP/iPWZ6xt9/5txf5iSQdGU
lbHNYwhD8wqe3gfXfN2rpj6WZyLLpMylcwbGECXqyzvLcp1wt+rkS/P4LGZ5jhk7
9WD39hs9WlX7ETe2QBvVcwtSEaNkJQ8oDiRWQivy4OMvZ8/KEUO6MORDaU6Jx9c7
NDkjCESpu6LgffvvlJajwUhuAJc0NoO35NBtaI0axOUKusxdEzP74k78ORtGUSIH
umKhtSFLpcId3jwip9iiqF/C9vMS8w9wIzCL/ehPF+R350rjjjZ2rvXJNHgEUuen
ZJz8K6FJOyPhC1hX+Qaef/poHblv+7nEzJcrxug1XjcYedzLkP0jf6oeir7BjKrL
tCt05Ce2DFm6/0jEMBjlAMnH7DT84rOCRxXN5/eYZQAilTuhKKzQi/aOffmIakph
G9Jo+iYfc+2Eac9I8xPJUZ3k8NyF+0sYJURVaSKNVPJk69Om912AW0pCjQJt5pxB
uZpQUmOmUdDkbb0Zb9SN0g0Y+Ou2BoFMI351GdXNWh1kToIGPf5o/6au1cGdAZQt
Iw6ydGzL/3GbG7mJmDXfnkb/30dgClXipa1xy1pENUyIhTa5XxPijAZCO//CkxPU
GARdszA9DM6VfyQKaaxru9Wk4NGywNh/tlMKBVh86pAEjeqn0l2Ycf6UhJy+ANJ3
NwahRh5/T9C3/C05/ZTTbETS1s7KVQwq0hG0meCLTyvtg+nVlhFP79ku159qw7Lz
pMbPlTctnedtCPdtvNq6L/oMXJ8WHsFt8ZUaQnr66mVxfAXlbxRbb3ViVKP9SJ40
cfOWCVkvdmhilUTeLV9uRfSIYRxVJk/KsKkg4sftYPOOfBmm50U5jRaWVgFNPBXS
nj+Ka0GvBxzxBRBAl9GSQa9zRArHdb+xf2fmIHoga4/e3Aoa5VtJRW6q7VRnqVKa
JXgPXZMRwt21QhEc1v3NhKxE80xjzClaKeWk9sPaCUz13/rJYbP1n1muUcfxflwL
4f9U/+Hykv3hhkbzJLZpy2s27nxxB6j2JarIWcIPcZpRAvB7O/epM14ImrB+6AJK
uc2ZZsYLfgHuZHhbmZZH/wCqVTs4gPglWvzIAbKqBx4JwhT9qpafkHBcdZoyTBWy
-----END RSA PRIVATE KEY-----

`

const initApp = () => {
  // Initializes the app, registers the button click listener
  const newUserButton = document.querySelector('#btnAddUser');
  newUserButton.addEventListener('click', addUser);
};

const addUser = () => {
  // Fetches the user input, creates a new HTML element based on it
  // and appends the element to the DOM
  const newUserNameInput = document.querySelector('input#name');
  const newUserAgeInput = document.querySelector('input#age');

  if (
    !validateInput(newUserNameInput.value, true, false) ||
    !validateInput(newUserAgeInput.value, false, true)
  ) {
    return;
  }

  const userList = document.querySelector('.user-list');
  const outputText = generateText(
    newUserNameInput.value,
    newUserAgeInput.value
  );
  const element = createElement('li', outputText, 'user-item');
  userList.appendChild(element);
};

// Start thehhhhh app!
initApp();
